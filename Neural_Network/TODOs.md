High Priority
=============

* read Batch Normalization Paper (https://arxiv.org/pdf/1502.03167.pdf)
* analyze BN paper into jupyter notebook
* debug accuracy reducing in MLP(tensorflow) notebook
* rewrite mlp with keras
* rewrite CNN with keras
* write ResNet with Keras
* write Inception with keras
* read the direct reference of BN paper
* read classic CNN papers

Low Priority
============

* what is the estimated center and scale, how can it be estimated
* use tf.nn.softmax_cross_entropy_with_logits_v2
* write debug_mode, tensor flow debugging